__author__ = "Hanna Krasowski"
__copyright__ = "TUM Cyber-Physical Systems Group"
__credits__ = ["ConVeY"]
__version__ = "0.1"
__maintainer__ = "Hanna Krasowski"
__email__ = "hanna.krasowski@tum.de"
__status__ = "Release"

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import os

LOCATION_DICT = {
"Florida": [27.51, 32.39, -80.18, -75.10],
"MiddleEastCoast": [35.25, 38.89, -74.96, -73.92],
"UpperWestCoast": [37.32, 48.56, -126.91, -124.85],
}

def data_cleaning(args, filename):
    """
    Data cleaning
    :param args: arguments for main
    :param filename: raw filename of MarineCadastre data
    :return: cleaned pandas dataframe
    """
    # Data cleaning
    data_frame = pd.read_csv(filename)
    print('Initial number of data points {} and unique ships {}'.format(len(data_frame.index), data_frame['MMSI'].value_counts().size))
    # Reduce Data to region
    if args.reduceLocation != []:

        if args.reduceLocation in LOCATION_DICT.keys():
            data_frame_w_loc = data_frame[((LOCATION_DICT[args.reduceLocation][0] <= data_frame['LAT']) &
                                               (LOCATION_DICT[args.reduceLocation][1] >= data_frame['LAT']) &
                                               (LOCATION_DICT[args.reduceLocation][2] <= data_frame['LON']) &
                                               (LOCATION_DICT[args.reduceLocation][3] >= data_frame['LON']) &
                                               (data_frame['Status'] == 0))]
            print('Predefined region usage reduced number of data points {} and unique ships {}'.format(
                len(data_frame_w_loc.index),
                data_frame_w_loc['MMSI'].value_counts().size))
        else:
            red = np.fromstring(args.reduceLocation, dtype= float, sep=',')
            data_frame_w_loc = data_frame[(data_frame['LAT'] >= red[0]) & (data_frame['LON'] >= red[2]) &
                                          (data_frame['LAT'] <= red[1]) & (data_frame['LON'] <= red[3])]
            print('Location specification reduced number of data points {} and unique ships {}'.format(len(data_frame_w_loc.index),
                                                                            data_frame_w_loc['MMSI'].value_counts().size))
    else:
        data_frame_w_loc = data_frame
    # Only use data points which have the fields length, width & vessel type
    data_frame_w_lwt = data_frame_w_loc[data_frame_w_loc['Length'].notnull() & data_frame_w_loc['Width'].notnull() &
                                  data_frame_w_loc['VesselType'].notnull()]
    # Only use ships with more than min_data_points
    data_frame_many_obs = data_frame_w_lwt.groupby('MMSI').filter(lambda x: len(x) > args.min_data_points)
    # Reduce Timeslot

    if args.reduceTimeframe != []:
        hour = int(args.reduceTimeframe[11:13]) + 1
        if hour <10:
            lastTime = args.reduceTimeframe[:11] + '0' + str(hour) + args.reduceTimeframe[13:]
        else:
            lastTime = args.reduceTimeframe[:11] + str(hour) + args.reduceTimeframe[13:]
        data_frame_time = data_frame_many_obs[(data_frame_many_obs['BaseDateTime']>=args.reduceTimeframe)
                                              & (data_frame_many_obs['BaseDateTime']< lastTime)]
    else:
        data_frame_time = data_frame_many_obs

    # Either only motor vessel or only relevant vessels
    if not args.only_motor_vessels:
        data_frame_with_specific_vt = data_frame_time[
            ((data_frame_many_obs['VesselType'] > 29) & (99 > data_frame_many_obs['VesselType']) | ((data_frame_many_obs['VesselType'] > 1000) & (1025 > data_frame_many_obs['VesselType'])))
            & (data_frame_many_obs['VesselType'] != 1008) & (
                    data_frame_many_obs['VesselType'] != 1009) & (data_frame_many_obs['VesselType'] != 1021)]
    else:
        data_frame_with_specific_vt = data_frame_time[
            (((data_frame_many_obs['VesselType'] > 29) & (99 > data_frame_many_obs['VesselType']) | ((data_frame_many_obs['VesselType'] > 1000) & (1025 > data_frame_many_obs['VesselType']))) &
            (data_frame_many_obs['VesselType'] != 1008) & (data_frame_many_obs['VesselType'] != 1009) &
            (data_frame_many_obs['VesselType'] != 1021) &
            (data_frame_many_obs['VesselType'] != 36) & (data_frame_many_obs['VesselType'] != 1019))]
    # Exclude all vessel which are not moving
    data_frame_moving = data_frame_with_specific_vt.groupby('MMSI').filter(lambda x: (x['SOG'] > args.threshold_no_moving).any())
    print('After cleaning number of data points {} and unique ships {}'.format(len(data_frame_moving.index),
                                                                        data_frame_moving['MMSI'].value_counts().size))
    return data_frame_moving

def find_ships_at_same_time_in_vicinity(ship_row, other_ship_row, time_range : pd.Timedelta,
                                        lat_range: float, lon_range: float):
    """
    function to check vicintity of two AIS samples
    :param ship_row: AIS sample of first ship
    :param other_ship_row: AIS sample of second ship
    :param time_range: maximum regarded time range
    :param lat_range: longitudinal max deviation
    :param lon_range:  lateral maximum deviation
    :return:
    """
    if (other_ship_row['BaseDateTime'] - time_range < ship_row['BaseDateTime'] < other_ship_row['BaseDateTime'] + time_range) and\
            (other_ship_row['LAT'] - lat_range < ship_row['LAT'] < other_ship_row['LAT'] + lat_range) and\
            (other_ship_row['LON'] - lon_range < ship_row['LON'] < other_ship_row['LON'] + lon_range):
        return True
    else:
        return False

def extract_AIS_scenarios(data, args, filename):
    """
    Extracting a dataframe which contains all AIS samples that are close
    :param data: pandas dataframe of cleaned MarineCadastre dat
    :param args: main function arguments
    :param filename: MarineCadastre data file name
    :return: dataframe which contains vicinity AIS data points
    """
    RANGE_DEV = args.vicinity_check_parameters[0]
    TIME_DEV = args.vicinity_check_parameters[1]
    data_groupes_MMSI = data.groupby('MMSI')
    number_of_vessels = data['MMSI'].value_counts().size

    # Create header for output dataframe
    header_list = data.columns.tolist()
    header_list_second = []
    for entry in header_list:
        header_list_second.append(entry + '_2')
    new_header = header_list + header_list_second
    data_samples_vicinity = pd.DataFrame(columns=new_header)
    visited_names = []
    idx = 0
    # Loop which checks every remaining ship (AIS points grouped by MMSI)
    for name, ship_samples in data_groupes_MMSI:
        idx += 1
        idx_other = 0
        print('Aggregating for ship {} of {}'.format(idx, number_of_vessels))
        # Loop for every other vessel except the combination which are already checked
        for name_other, other_ship_samples in data_groupes_MMSI:
            idx_other += 1
            if (name != name_other) and (name_other not in visited_names):
                # Check if the coordinate ranges overlap by min and max
                min_t_ship = ship_samples['BaseDateTime'].min()
                max_t_ship = ship_samples['BaseDateTime'].max()
                min_t_other = other_ship_samples['BaseDateTime'].min()
                max_t_other = other_ship_samples['BaseDateTime'].max()
                if ((min_t_other - pd.Timedelta(minutes=TIME_DEV) > max_t_ship) or
                     (max_t_other + pd.Timedelta(minutes=TIME_DEV) < min_t_ship)):
                    continue
                else:
                    # If yes, check if samples in vicinity
                    row_iterator = ship_samples.iterrows()
                    _, last = next(row_iterator)  # Take first item from row_iterator
                    for i, rows_ship in row_iterator:
                        row_iterator_other = other_ship_samples.iterrows()
                        _, last_other = next(row_iterator_other)
                        for i, rows_other in row_iterator_other:
                            if find_ships_at_same_time_in_vicinity(rows_ship, rows_other,
                                                                   pd.Timedelta(minutes=TIME_DEV),
                                                                   RANGE_DEV, RANGE_DEV):
                                new_row = pd.DataFrame(
                                    pd.concat([rows_ship, rows_other], ignore_index=True)).T
                                new_row.columns = new_header
                                data_samples_vicinity = data_samples_vicinity.append(new_row, ignore_index=True)
        print('Length of vicinity dataset {}'.format(len(data_samples_vicinity.index)))
        
        # Add checked vessel to visited list
        visited_names.append(name)

    return data_samples_vicinity

def MarineCadastre2AISscenario(args, filename):
    """
    Searching for raw AIS encounters
    :param args: arguments for main
    :param filename: raw filename of MarineCadastre data
    """
    # Data cleaning
    data_cleaned = data_cleaning(args, filename)
    # Data adjustment
    data_cleaned['BaseDateTime'] = pd.to_datetime(data_cleaned['BaseDateTime'], format='%Y-%m-%d %H:%M:%S')

    # Save without checking vicinity
    if not args.vicinity_check:
        assert args.reduceLocation != [] and args.reduceLocation not in LOCATION_DICT.keys(), \
            'Error: AIS data of all vessels can only be stored for specified and small location'
        loc = args.reduceLocation.split(',')
        start = args.reduceTimeframe.split('T')[1].split(':')
        output_name = filename.split('.')[0] + '_' + loc[0] + '-'+ loc[1] + '_' + start[0]+'_'+ '_scenario.csv'
        data_cleaned.to_csv(output_name)
    else:
        # Extract AISscenarios and put in new folder
        data_mapped = extract_AIS_scenarios(data_cleaned, args, filename)
        
        # Save file with mapping
        if args.reduceLocation != []:
            output_name = filename.split('.')[0] + '_' + args.reduceLocation + '_scenarios.csv'
        else:
            output_name = filename.split('.')[0] + '_scenarios.csv'
        data_mapped.to_csv(output_name, index=False, header=True)
    pass
