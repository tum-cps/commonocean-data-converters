__author__ = "Hanna Krasowski"
__copyright__ = "TUM Cyber-Physical Systems Group"
__credits__ = ["ConVeY"]
__version__ = "0.1"
__maintainer__ = "Hanna Krasowski"
__email__ = "hanna.krasowski@tum.de"
__status__ = "Release"

import datetime

import numpy as np
import utm
from typing import List, Union
import pandas as pd
import os
import datetime as dte

from commonocean.scenario.scenario import Scenario, DynamicObstacle, ObstacleType, Location, Tag
from commonocean.prediction.prediction import TrajectoryPrediction, Trajectory
from commonocean.scenario.trajectory import State
from commonocean.common.file_writer import CommonOceanFileWriter, OverwriteExistingFile
from commonocean.planning.planning_problem import PlanningProblemSet, PlanningProblem
from commonocean.scenario.waters import WatersNetwork
from commonocean.planning.goal import GoalRegion

from commonroad.geometry.shape import Rectangle, Circle
from commonroad.common.util import Interval

SOURCE = 'Marine Cadastre'


def kn2meterpersecond(knot: Union[float, np.array]) -> Union[float, np.array]:
    return 0.5144 * knot

def COG2heading(COG: float) -> float:
    raw_angle = ((-COG + 90)/360) * 2 * np.pi
    return raw_angle

def wrap_angle(raw_angle: float) -> float:
    while not (0 <= raw_angle < (2 * np.pi)):
        if raw_angle < 0:
            raw_angle += 2 * np.pi
        else:
            raw_angle -= 2 * np.pi
    return raw_angle

def linear_interpolation(start: float, stop: float, fraction: float) -> float:
    return start + (fraction * (stop - start))

def linear_interpolation_angle(start: float, stop: float, fraction: float) -> float:
    start_wrapped = wrap_angle(start)
    stop_wrapped = wrap_angle(stop)
    if abs(stop_wrapped - start_wrapped) > np.pi:
        delta = 2* np.pi - abs(stop_wrapped - start_wrapped)
    else:
        delta = abs(stop_wrapped - start_wrapped)
    if (start_wrapped > stop_wrapped and stop_wrapped - start_wrapped < 0) or (start_wrapped < stop_wrapped and stop_wrapped - start_wrapped > np.pi):
        delta = -1 * delta

    return start + (fraction * delta)

def create_shape(width: float, length: float) -> Rectangle:
    return Rectangle(length, width)

def get_obstacle_type(VesselType: int) -> ObstacleType:
    if VesselType ==36 or VesselType==37 or VesselType ==1019:
        return ObstacleType.SAILINGVESSEL
    elif VesselType == 30 or VesselType == 1001 or VesselType == 1002:
        return ObstacleType.FISHINGVESSEL
    elif VesselType == 35:
        return ObstacleType.MILITARYVESSEL
    elif 79 >= VesselType >= 70 or 1004 >= VesselType >= 1003 or VesselType == 1016:
        return ObstacleType.CARGOSHIP
    elif 50 > VesselType > 39 or 90 > VesselType > 59 and 1017 >= VesselType >= 1012:
        return ObstacleType.MOTORVESSEL
    elif (99 > VesselType > 29 or 1025 > VesselType > 1000) and VesselType != 1008 and VesselType != 1009 and VesselType != 1021:
        return ObstacleType.UNKNOWN

def AIS2State(initial_time,dt:float, data, cosy_center: np.array, zone) -> List[State]:
    """
    Creates CR states from AIS data
    :param initial_time: start time of scenario
    :param dt: time step size of scenario
    :param data: all relevant AIS data points from one vessel in the respective time interval
    :param cosy_center: center of scenario / coordinate system
    :param zone: utm conversion zone
    :return: CR state list
    """
    state_list = []
    state_list_not_interpolated = []
    i = 0
    initial_time_numeric = dte.datetime.strptime(initial_time, '%Y-%m-%d %H:%M:%S')
    times = data['BaseDateTime'].to_list()
    times_list = []
    for time in times:
        times_list.append(time - initial_time_numeric)
    state_list_AIS = [ times_list,
                      data['LAT'].to_numpy(), data['LON'].to_numpy(), data['COG'].to_numpy(), data['SOG'].to_numpy()]
    time_step = int(state_list_AIS[0][0].total_seconds() / dt)
    N = int((state_list_AIS[0][-1].total_seconds() - state_list_AIS[0][0].total_seconds()) / dt)
    velocity_array = kn2meterpersecond(state_list_AIS[4])
    gps_position_array = np.vstack((state_list_AIS[1].transpose(), state_list_AIS[2].transpose()))
    utm_pos = utm.from_latlon(gps_position_array[0][i], gps_position_array[1][i], force_zone_number=zone[0], force_zone_letter=zone[1])
    position = np.array([utm_pos[0], utm_pos[1]]) - cosy_center
    initial_state = State(position=position, velocity=velocity_array[i], orientation=COG2heading(state_list_AIS[3][i]), time_step=time_step+i)
    
    i += 1
    for j in range(len(state_list_AIS[0])-1):
        position = utm.from_latlon(gps_position_array[0][i], gps_position_array[1][i], force_zone_number=zone[0],
                                   force_zone_letter=zone[1])
        state = State(position=np.array([position[0], position[1]]) - cosy_center, velocity=velocity_array[i],
                      orientation=COG2heading(state_list_AIS[3][i]), time_step=int(state_list_AIS[0][i].total_seconds() / dt))
        state_list_not_interpolated.append(state)
        i += 1

    previous_state = initial_state
    t = previous_state.time_step
    N = int(state_list_AIS[0][-1].total_seconds() / dt)
    while t <= N:
        if state_list_not_interpolated:
            for not_interpolated_state in state_list_not_interpolated:
                t_next = not_interpolated_state.time_step
                while t <= t_next:
                    if t == t_next:
                        state_list.append(not_interpolated_state)
                        previous_state = not_interpolated_state
                        t += 1
                        break
                    else:
                        fraction = (t - previous_state.time_step) / (t_next - previous_state.time_step)
                        position = np.array([linear_interpolation(previous_state.position[0], not_interpolated_state.position[0], fraction),
                                            linear_interpolation(previous_state.position[1], not_interpolated_state.position[1], fraction)])
                        orientation = linear_interpolation_angle(previous_state.orientation, not_interpolated_state.orientation, fraction)
                        velocity = linear_interpolation(previous_state.velocity, not_interpolated_state.velocity, fraction)
                        state = State(position=position, velocity=velocity,
                                    orientation=orientation,
                                    time_step=t)
                        state_list.append(state)
                        t += 1
        else:
            break

    for key, state in enumerate(state_list):
        try:
            vector = state_list[key+1].position - state.position
            i = 1
            while np.linalg.norm(vector) == 0:
                i += 1
                vector = state_list[key+i].position - state.position
            vector_norm = vector / np.linalg.norm(vector)
            state_list[key].orientation = wrap_angle(np.arctan2(vector_norm[1], vector_norm[0]))
        except IndexError:
            state_list[key].orientation = state_list[key-1].orientation

    return state_list

def create_obstacle(MMSI: int, width: float, length: float, states: list, obstacletype: ObstacleType) -> DynamicObstacle:
    """
    function to create a CR dynamic obstacle
    :param MMSI: MMSI of ship - used as id
    :param width: width of ship
    :param length: length of ship
    :param states: list of states, while first state is initial state
    :param obstacletype: Obstacle Type
    :return: CommonRoad dynamic obstacle
    """
    obstacle_shape = create_shape(width, length)
    initial_state = states[0]
    trajectory = Trajectory(initial_time_step=states[0].time_step, state_list=states[0:])
    prediction = TrajectoryPrediction(trajectory=trajectory, shape=obstacle_shape)

    obstacle = DynamicObstacle(obstacle_id=int(MMSI), obstacle_shape=obstacle_shape, obstacle_type= obstacletype,
                               initial_state=initial_state, prediction=prediction)
    return obstacle

def create_scenario(original_data, output_dir, total_ships, filename, args):
    """
    :param original_data: original Marine Cadastre data
    :param output_dir: directory to store the scenarios
    :param total_ships: counter - just as info needed
    :return: total ships counter to cumulate for one day
    """
    i = 0

    if args.author == '':
        AUTHOR = 'John Doe'
    else:
        AUTHOR = str(args.author)

    if args.affiliation == '':
        AFFILIATION = 'Technical University of Munich, Germany'
    else:
        AFFILIATION = str(args.affiliation)

    if args.tag == '':
        TAGS = ''
    else:
        TAGS = [Tag(args.tag)]

    if args.dt <= 0.0:
        DT = 1.0
    else:
        DT = float(args.dt)

    original_data['BaseDateTime'] = pd.to_datetime(original_data['BaseDateTime'], format='%Y-%m-%d %H:%M:%S')
    data_groupes_MMSI = original_data.groupby('MMSI')
    initial_time = str(min(original_data['BaseDateTime']))

    gps_latitude = data_groupes_MMSI.get_group((list(data_groupes_MMSI.groups)[0]))['LAT'].iloc[0]
    gps_longitude = data_groupes_MMSI.get_group((list(data_groupes_MMSI.groups)[0]))['LON'].iloc[0]

    center = utm.from_latlon(gps_latitude, gps_longitude)
    zone = (center[2], center[3])
    cosy_center = np.array([center[0], center[1]])
    obstacles = []
    MMSIship = args.MMSI
    for i in range(0, data_groupes_MMSI.ngroups):
        MMSI = list(data_groupes_MMSI.groups.keys())[i]
        if (MMSI != MMSIship):
            data = data_groupes_MMSI.get_group(MMSI)
            states = AIS2State(initial_time=initial_time, dt=DT, data=data, cosy_center=cosy_center, zone=zone)
            if states:
                width = data['Width'].to_list()[0]
                length = data['Length'].to_list()[0]
                obstacletype = get_obstacle_type(data['VesselType'].to_list()[0])
                new_obstacle = create_obstacle(MMSI=MMSI,width=width, length=length, states=states, obstacletype=obstacletype)
                obstacles.append(new_obstacle)

    if len(obstacles) >= 2:
        if len(obstacles) > 2:
            print("Scenario with {} obstacles".format(len(obstacles)))

        date_list = initial_time.split(' ')[0].split('-')
        date = date_list[0] + date_list[1] + date_list[2]
        id = "ZAM_{0}_{1}_T-{2}".format(args.scene, date, i + 1)
        scenario = Scenario(dt=DT, scenario_id=id)
        scenario.add_objects(obstacles)

        flag = True
        for dyn_obst in scenario.dynamic_obstacles:
            if not flag:
                temp_limits = dyn_obst.extreme_limits()
                if temp_limits[0][0] > limits [0][0]:
                    limits[0][0] = temp_limits[0][0].copy()
                if temp_limits[1][0] > limits [1][0]:
                    limits[1][0] = temp_limits[1][0].copy()
                if temp_limits[0][1] < limits [0][1]:
                    limits[0][1] = temp_limits[0][1].copy()
                if temp_limits[1][1] < limits [1][1]:
                    limits[1][1] = temp_limits[1][1].copy()
            else:
                limits = dyn_obst.extreme_limits()
                flag = False

        tolerance = 1000*(2)
        
        scenario.add_objects(WatersNetwork(np.array([round(sum(limits[0])/len(limits[0]),2),round(sum(limits[1])/len(limits[1]),2)]), round(limits[0][0] - limits[0][1] + tolerance,2), round(limits[1][0] - limits[1][1] + tolerance,2), 0.0))

        if MMSIship != 0:
            data= data_groupes_MMSI.get_group(MMSIship)
            times_list = data['BaseDateTime'].to_list()
            state_list_AIS = [times_list,
                              data['LAT'].to_numpy(), data['LON'].to_numpy(), data['COG'].to_numpy(),
                              data['SOG'].to_numpy()]
            velocity_array = kn2meterpersecond(state_list_AIS[4])
            gps_position_array = np.vstack((state_list_AIS[1].transpose(), state_list_AIS[2].transpose()))
            delta_time = datetime.timedelta(0,DT)
            end = times_list[-1] + delta_time
            utm_pos = utm.from_latlon(gps_position_array[0][0], gps_position_array[1][0], force_zone_number=zone[0],
                                      force_zone_letter=zone[1])
            position = np.array([utm_pos[0], utm_pos[1]]) - cosy_center
            inital_state= State(position=position, velocity=velocity_array[0], orientation=COG2heading(state_list_AIS[3][0]),time_step=int(DT))
            utm_pos = utm.from_latlon(gps_position_array[0][-1], gps_position_array[1][-1], force_zone_number=zone[0],
                                      force_zone_letter=zone[1])
            position = Circle(100, np.array([utm_pos[0], utm_pos[1]]) - cosy_center)
            goal_state = State(position=position, time_step = Interval(int(times_list[-1].strftime('%s')), int(end.strftime('%s'))))
            goal_region = GoalRegion([goal_state])
            problem = PlanningProblem(1, inital_state, goal_region)
            planning_problem_set = PlanningProblemSet([problem])

        else:
            planning_problem_set = PlanningProblemSet()
        geolocation = Location(geo_name_id=-999, gps_latitude=gps_latitude, gps_longitude=gps_longitude)
        fw = CommonOceanFileWriter(scenario=scenario, planning_problem_set=planning_problem_set,
                                  author=AUTHOR, affiliation=AFFILIATION, source=SOURCE, tags=TAGS,location=geolocation)
        filename = filename.split('.csv')[0] + '.xml'
        filename = filename.split('/')[-1]
        fw.write_to_file(output_dir + '/' +filename, OverwriteExistingFile.ALWAYS)
        print("Scenario file stored in {}".format(output_dir + '/' + filename))
        total_ships += len(obstacles)

        i += 1
    return total_ships
