""" Converter for MarineCadastre data set to CommonOcean """

__author__ = "Bruno Maione"
__copyright__ = "TUM Cyber-Physical Systems Group"
__credits__ = ["ConVeY"]
__version__ = "0.1"
__maintainer__ = "Hanna Krasowski"
__email__ = "hanna.krasowski@tum.de"
__status__ = "Release"

import argparse
from typing import Union
import os
import pandas as pd

from MarineCadastre2AISscenario import MarineCadastre2AISscenario, LOCATION_DICT
from AISscenario2co import create_scenario

def get_args():

    parser = argparse.ArgumentParser(description="Generation of dynamic obstacles from AIS data for CommonOcean scenarios")
    # relevant for filtering step
    parser.add_argument('--only_motor_vessels', default=True, type=bool,
                        help='<Filtering step>: True if only motor vessels should be considered')
    parser.add_argument('--threshold_no_moving', default=0.4, type=float,
                        help='<Filtering step>: Only considers vessels for which at least one time the velocity in '
                             'knots is higher than the threshold value')
    parser.add_argument('--min_data_points', default=144, type=int,
                        help='Minimum of datapoints for one vessel to be considered')
    parser.add_argument('--generate_AIS_raw', default=False, type=bool,
                        help='<Filtering step>: True if raw encounter should be found, false to generate '
                             'CommonOcean scenarios from raw encounter')
    parser.add_argument('--vicinity_check', default=True, type=bool,
                        help='<Filtering step>: True if it should be searched for AIS data points of '
                             'different ships that are close,'
                             'false to store all AIS data')
    parser.add_argument('--vicinity_check_parameters', default=[0.03, 10], type=list,
                        help='<Filtering step>: Parameters for vicinity check [max distance in degree, '
                             'min time for vicinity in minutes]')
    parser.add_argument('--reduceLocation', default=[], type=str,
                        help='<Filtering step>: Reduce the data to certain locations, given with x1,x2,y1,y2 or '
                             'as predefined region: Florida, MiddleEastCoast, UpperWestCoast, NovaScotia')
    parser.add_argument('--reduceTimeframe', default=[], type=str, help='<Filtering step>: Reduce used timeframe, '
                                                                        'from given time + 1h, time given as '
                                                                        'Year-Month_DayTHour:Minute:Second')
    # relevant for xml generation
    parser.add_argument('--MMSI', default=0, type=int, help='<XML generation step>: choose ship which you want '
                                                               'to exclude from the scenario generation by MMSI-number;'
                                                               ' if set to 0 no ship is excluded')
    parser.add_argument('--scene', type=str,
                        help='<XML generation step>: scene identifier; format: XXX-0'
                             ' e.g. FLO-1 for open-sea area Florida')
    parser.add_argument('--author', type=str,
                        help='<XML generation step>: name of the author of the new scenario')
    parser.add_argument('--affiliation', type=str,
                        help='<XML generation step>: name of the affiliation of the new scenario')
    parser.add_argument('--tag', default='', type=str,
                        help='<XML generation step>: tag to be inserted in the new scenario')
    parser.add_argument('--dt', default=10.0, type=float,
                        help='<XML generation step>: dt step of the new scenario')
    # relevant for both steps
    parser.add_argument('--scenario_path', default='../../data/MarineCadastre/AIS_2019_01_01.csv', type=str,
                        help='<Both steps>: Path to scenario to consider.')

    return parser.parse_args()


def main():
    args = get_args()
    # Generates raw encounters which contain AIS samples of two ships in vicinity
    if args.generate_AIS_raw:
        filename = args.scenario_path
        if filename.endswith(".csv"):
            MarineCadastre2AISscenario(args, filename)
    # Generate single CommonOcean scenario from AIS raw encounters
    else:
        total_ships = 0
        filename = args.scenario_path
        path = '/'.join(filename.split('/')[:-1])
        if filename.endswith('.csv'):
            data_original = pd.read_csv(filename)
            output_dir = path
            total_ships = create_scenario(data_original, output_dir, total_ships, filename, args)
        else:
                print('Error: location not implemented')
        print("TOTAL: {} vessels".format(total_ships))

    pass

if __name__ == "__main__":
    main()
