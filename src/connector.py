""" Connect Marine Cadastre files with OpenSeaMap files"""

__author__ = "Bruno Maione"
__copyright__ = "TUM Cyber-Physical Systems Group"
__credits__ = ["ConVeY"]
__version__ = "0.1"
__maintainer__ = "Hanna Krasowski"
__email__ = "hanna.krasowski@tum.de"
__status__ = "Release"

import argparse
import numpy as np
from commonocean.common.file_reader import CommonOceanFileReader
from commonocean.common.file_writer import CommonOceanFileWriter, OverwriteExistingFile
from commonocean.scenario.scenario import Tag

SOURCE = 'Marine Cadastre and OpenSeaMap' 

def get_args():

    parser = argparse.ArgumentParser(description="Script that connects both the MarineCadastre dynamic data and OpenSeaMap static information")
    parser.add_argument('--open_sea_map_path', default='../../data/MarineCadastre/MC_2019_01_01.xml', type=str,
                        help='Path to the OpenSeaMap XML scenario to consider')
    parser.add_argument('--marine_cadastre_path', default='../../data/MarineCadastre/AIS_2019_01_01_scenarios.xml', type=str,
                        help='Path to the MarineCadastre XML scenario to consider')
    parser.add_argument('--output_path', default='/home', type=str, help='Path to the folder of the output')
    parser.add_argument('--author', default='John Doe', type=str,
                        help='name of the author of the new scenario')
    parser.add_argument('--affiliation', default='Technical University of Munich, Germany', type=str,
                        help='name of the affiliation of the new scenario')
    parser.add_argument('--tag', default='', type=str,
                        help='tag to be inserted in the new scenario')

    return parser.parse_args()

def main():
    args = get_args()

    osm_scenario, osm_planning_problem_set = CommonOceanFileReader(args.open_sea_map_path).open()
    osm_center = osm_scenario._waters_network.navigationable_area.center
    osm_scenario.translate_rotate(np.array([-osm_center[0], -osm_center[1]]), 0.0)

    mc_scenario, mc_planning_problem_set = CommonOceanFileReader(args.marine_cadastre_path).open()
    mc_center = mc_scenario._waters_network.navigationable_area.center
    mc_scenario.translate_rotate(np.array([-mc_center[0], -mc_center[1]]), 0.0)

    if osm_scenario.dt != mc_scenario.dt:
        raise ValueError("Scenarios don't have the same dT (timeStepSize)")

    if args.tag == '':
        TAGS = ''
    else:
        TAGS = [Tag(args.tag)]

    obstacles_to_transfer = osm_scenario.obstacles + osm_scenario._waters_network.traffic_signs
    mc_scenario.add_objects(obstacles_to_transfer, waters_ids=[])

    filewriter = CommonOceanFileWriter(mc_scenario, mc_planning_problem_set, args.author, args.affiliation, SOURCE, TAGS)
    filewriter.write_to_file(args.output_path + '/Fusion_OpenSeaMap_MarineCadastre.xml', OverwriteExistingFile.ALWAYS)
    return args.output_path + '/Fusion_OpenSeaMap_MarineCadastre.xml'

if __name__ == "__main__":
    main()
