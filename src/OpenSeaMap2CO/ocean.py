import utm
import numpy as np
import xml.etree.ElementTree as ElTree
from ordered_set import OrderedSet
from commonroad.geometry.shape import Polygon, Circle
from commonocean.scenario.scenario import Scenario, Location
from commonocean.scenario.obstacle import StaticObstacle, ObstacleType
from commonocean.scenario.traffic_sign import TrafficSign,TrafficSignElement, TrafficSignElementID
from commonocean.scenario.waters import WatersType, Waterway, Shallow, WatersNetwork
from commonocean.common.file_writer import CommonOceanFileWriter
from commonocean.planning.planning_problem import PlanningProblemSet
from commonocean.scenario.state import State
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap
import scipy.cluster.hierarchy as hcluster
from scipy.spatial import ConvexHull

def convert_seamap(filename, dt, output_path, precision, clustering):
    """

    finds Buoys in OSM and converts them to a CommonOcean scenario

    """
    tree = ElTree.parse(filename)
    root = tree.getroot()
    nodes = root.iter('node')
    buoys =  OrderedSet()

    for node in nodes:
        for tag in node.iter('tag'):
            if 'seamark:buoy' in tag.attrib['k']:
                buoys.add(node)
                break

    ways = root.iter('way')
    fairways = OrderedSet()
    coastlines = OrderedSet()

    for way in ways:
        for tag in way.iter('tag'):
            if tag.attrib['k'] == 'seamark:type' and (tag.attrib['v'] == 'fairway' or  tag.attrib['v'] == 'separation_zone'):
                fairways.add(way)
                break
            if tag.attrib['k'] == 'natural' and tag.attrib['v'] == 'coastline':
                coastlines.add(way)
                break

    bounds_list = root.iter('bounds')
    for bound_gps in bounds_list:
        loc = Location(gps_latitude = (float(bound_gps.attrib['maxlat']) + float(bound_gps.attrib['minlat']))/2, gps_longitude = (float(bound_gps.attrib['maxlon']) + float(bound_gps.attrib['minlon']))/2)

    scenario = Scenario(dt, 1, location = loc)

    if len(fairways) > 0:
        addWaters(fairways, scenario, root)
    else:
        bounds_list = root.iter('bounds')
        for bounds in bounds_list:
            corners, zone_data = addWatersNetworkGeneral(scenario, bounds)
    if len(coastlines) > 0:
        addLand(coastlines,scenario, root, corners, zone_data, precision, clustering)
    if len(buoys) > 0:
        addBuoys(buoys,scenario)

    center = scenario._waters_network.navigationable_area.center
    scenario.translate_rotate(np.array([-center[0], -center[1]]), 0.0)

    filewriter = CommonOceanFileWriter(scenario, PlanningProblemSet(), "John Doe", "TUM", "OpenSeaMap", "")
    filewriter.write_to_file(output_path + "OSM_Scenario.xml")


def getType(buoy) -> str:
    """
    sorts the OSM buoy types to their equals in CommonOcean
    """

    for tag in buoy.iter('tag'):
        if 'seamark:buoy_cardinal:category' in tag.attrib['k']:
            if tag.attrib['v'] == 'north':
                return '104'
            elif tag.attrib['v'] == 'east':
                return '105'
            elif tag.attrib['v'] == 'south':
                return '106'
            elif tag.attrib['v'] == 'west':
                return '107'
        elif 'seamark:buoy_installation' in tag.attrib['k']:
            return '103'
        elif 'seamark:buoy_isolated_danger' in tag.attrib['k']:
            return '103'
        elif 'seamark:buoy_lateral:colour' in tag.attrib['k']:
            if tag.attrib['v'] == 'red':
                return '101'
            elif tag.attrib['v'] == 'green':
                return '102'
        elif 'seamark:buoy_safe_water' in tag.attrib['k']:
            return '103'
        elif 'seamark:buoy_special_purpose' in tag.attrib['k']:
            return '103'
    return '103'

def addWatersNetworkGeneral(scenario: Scenario, bound):
    max_lat = bound.attrib['maxlat']
    min_lat = bound.attrib['minlat']
    max_lon = bound.attrib['maxlon']
    min_lon = bound.attrib['minlon']
    
    coordMaxMax = list(utm.from_latlon(float(max_lat), float(max_lon))[:2])
    coordMaxMin = list(utm.from_latlon(float(max_lat), float(min_lon))[:2])
    coordMinMax = list(utm.from_latlon(float(min_lat), float(max_lon))[:2])
    coordMinMin = list(utm.from_latlon(float(min_lat), float(min_lon))[:2])
    zone_data = list(utm.from_latlon(float(max_lat), float(max_lon))[2:])

    corners = [coordMaxMax, coordMaxMin, coordMinMax, coordMinMin]
    x = [p[0] for p in corners]
    y = [p[1] for p in corners]
    center = [sum(x)/len(corners), sum(y)/len(corners)]

    dimensions = [x - y for x, y in zip(coordMaxMax, coordMinMin)]

    waters_network = WatersNetwork(np.array(center), abs(float(dimensions[0])), abs(float(dimensions[1])), 0.0)
    scenario.replace_waters_network(waters_network)

    return corners, zone_data

def addLand(lands, scenario: Scenario, root, corners, zone_data, precision, clustering):
    """
    adds coastlines to scenario
    """
    lines_not_closed = []
    max_edge_ref = 5000 

    for land in lands: 
        z=[]
        zone = []
        for nd in land.iter('nd'):
            xpath = "./node[@id='{index}']".format(index=nd.attrib['ref'])
            node = root.find(xpath)

            coordinates = utm.from_latlon(float(node.get('lat')), float(node.get('lon')))
            z.append([coordinates[0],coordinates[1]])

            UTMZone = [coordinates[2], coordinates[3]]
            if zone == []:
                zone = [coordinates[2], coordinates[3]]
            else:
                assert (zone[0] == UTMZone[0] and zone[1] == UTMZone[1]), 'Coordinates are in different UTM zones, {} != {}'.format(zone, UTMZone)


        if(len(z)>=3):
            if z[0] != z[-1]:
                lines_not_closed.append(z)
            else:
                init_state_obs = State(time_step=0, orientation=0, position=np.array([0, 0]), velocity=0)
                shape_obs = Polygon(np.array(z))
                static_obs = StaticObstacle(Scenario.generate_object_id(scenario), ObstacleType.LAND, obstacle_shape=shape_obs, initial_state=init_state_obs)
                scenario.add_objects(static_obs)
    
    threshold = 5
    static = False 
    while not static:
        new_lines = []
        used_lines = []
        for line in lines_not_closed:
            connected = False
            for line2 in lines_not_closed:
                if line not in used_lines:
                    if line != line2:
                        if ((line[0][0] - line2[0][0])**2 + (line[0][1] - line2[0][1])**2)**(0.5) < threshold:
                            new_lines.append(list(reversed(line)) + line2)
                            connected = True
                        if ((line[-1][0] - line2[0][0])**2 + (line[-1][1] - line2[0][1])**2)**(0.5) < threshold:
                            new_lines.append(line + line2)
                            connected = True
                        if ((line[-1][0] - line2[-1][0])**2 + (line[-1][1] - line2[-1][1])**2)**(0.5) < threshold:
                            new_lines.append(line + list(reversed(line2)))
                            connected = True
                        if ((line[0][0] - line2[-1][0])**2 + (line[0][1] - line2[-1][1])**2)**(0.5) < threshold:
                            new_lines.append(list(reversed(line)) + list(reversed(line2)))
                            connected = True
                        if connected:
                            used_lines.append(line)
                            used_lines.append(line2)
                            
            if not connected and line not in used_lines:
                new_lines.append(line)
                used_lines.append(line)

        if len(lines_not_closed) == len(new_lines):
            static = True
        else:
            lines_not_closed = new_lines.copy()

    x = [p[0] for p in corners]
    y = [p[1] for p in corners]
    center = [sum(x)/len(corners), sum(y)/len(corners)]
    coordMaxMax = corners[0]
    coordMinMin = corners[-1]
    dimensions = [(x - y)/2 for x, y in zip(coordMaxMax, coordMinMin)]

    for line in new_lines:
        initial_point = line[0]
        last_point = line[-1]

        cross_up_right = False
        cross_up_left = False

        cross_bottom_right = False
        cross_bottom_left = False

        cross_right_up = False
        cross_right_bottom = False
        
        cross_left_up = False
        cross_left_bottom = False

        blind_zone_top_right = False
        blind_zone_top_left = False
        blind_zone_bottom_left = False
        blind_zone_bottom_right = False

        if (initial_point[1] >= coordMaxMax[1] and initial_point[0] >= center[0]) or (last_point[1] >= coordMaxMax[1] and last_point[0] >= center[0]):
            cross_up_right = True
        if (initial_point[1] >= coordMaxMax[1] and initial_point[0] < center[0]) or (last_point[1] >= coordMaxMax[1] and last_point[0] < center[0]):
            cross_up_left = True

        if (initial_point[1] <= coordMinMin[1] and initial_point[0] >= center[0]) or (last_point[1] <= coordMinMin[1] and last_point[0] >= center[0]):
            cross_bottom_right = True
        if (initial_point[1] <= coordMinMin[1] and initial_point[0] < center[0]) or (last_point[1] <= coordMinMin[1] and last_point[0] < center[0]):
            cross_bottom_left = True

        if (initial_point[0] >= coordMaxMax[0] and initial_point[1] >= center[1]) or (last_point[0] >= coordMaxMax[0] and last_point[1] >= center[1]):
            cross_right_up = True
        if (initial_point[0] >= coordMaxMax[0] and initial_point[1] < center[1]) or (last_point[0] >= coordMaxMax[0] and last_point[1] < center[1]):
            cross_right_bottom = True

        if (initial_point[0] <= coordMinMin[0] and initial_point[1] >= center[1]) or (last_point[0] <= coordMinMin[0] and last_point[1] >= center[1]):
            cross_left_up = True
        if (initial_point[0] <= coordMinMin[0] and initial_point[1] < center[1]) or (last_point[0] <= coordMinMin[0] and last_point[1] < center[1]):
            cross_left_bottom = True

        if (initial_point[0] >= coordMaxMax[0] and initial_point[1] >= coordMaxMax[1]) or (last_point[0] >= coordMaxMax[0] and last_point[1] >= coordMaxMax[1]):
            blind_zone_top_right = True
        if (initial_point[0] >= coordMaxMax[0] and initial_point[1] <= coordMinMin[1]) or (last_point[0] >= coordMaxMax[0] and last_point[1] <= coordMinMin[1]):
            blind_zone_bottom_right = True
        if (initial_point[0] <= coordMinMin[0] and initial_point[1] >= coordMaxMax[1]) or (last_point[0] >= coordMinMin[0] and last_point[1] >= coordMaxMax[1]):
            blind_zone_top_left = True
        if (initial_point[0] <= coordMinMin[0] and initial_point[1] <= coordMinMin[1]) or (last_point[0] >= coordMinMin[0] and last_point[1] <= coordMinMin[1]):
            blind_zone_bottom_left = True
        
        if (cross_up_right and cross_right_up) and not blind_zone_top_right:
            line.append(coordMaxMax)
            line.append(line[0])
        elif (cross_up_left and cross_left_up) and not blind_zone_top_left:
            line.append(corners[1])
            line.append(line[0])
        elif (cross_bottom_left and cross_left_bottom) and not blind_zone_bottom_left:
            line.append(coordMinMin)
            line.append(line[0])
        elif (cross_bottom_right and cross_right_bottom) and not blind_zone_bottom_right:
            line.append(corners[2])
            line.append(line[0])

        if line[0] != line[-1]:         
            if ((line[0][0] - line[-1][0])**2 + (line[0][1] - line[-1][1])**2)**(0.5) > threshold:
                line.append(line[0])
            else:
                line[-1] = line[0]
        
        init_state_obs = State(time_step=0, orientation=0, position=np.array([0, 0]), velocity=0)
        shape_obs = Polygon(np.array(line))
        static_obs = StaticObstacle(Scenario.generate_object_id(scenario), ObstacleType.LAND, obstacle_shape=shape_obs, initial_state=init_state_obs)
        scenario.add_objects(static_obs)

    if lines_not_closed:
        print("Loading adjustments...")
        lat, lon = utm.to_latlon(coordMaxMax[0], coordMaxMax[1], zone_data[0], zone_data[1])
        bm = Basemap(resolution='f', llcrnrlon = max(lon - 5, -179.9), llcrnrlat = max(lat - 5, -89.9), 
               urcrnrlon = min(lon + 5, 179.9), urcrnrlat = max(lat + 5, 89.9))

        scatter_plot = []
        for x in range(precision):
            for y in range(precision):
                xpt = ((coordMaxMax[0] - coordMinMin[0])/precision)*x + coordMinMin[0]
                ypt = ((coordMaxMax[1] - coordMinMin[1])/precision)*y + coordMinMin[1]
                bm_xpt, bm_ypt = utm.to_latlon(xpt, ypt, zone_data[0], zone_data[1])
                if bm.is_land(bm_ypt, bm_xpt):
                    if not clustering:
                        init_state_obs = State(time_step=0, orientation=0, position=np.array([0, 0]), velocity=0)
                        shape_obs = Circle(max_edge_ref/precision, center = np.array([xpt,ypt]))
                        static_obs = StaticObstacle(Scenario.generate_object_id(scenario), ObstacleType.LAND, obstacle_shape=shape_obs, initial_state=init_state_obs)
                        scenario.add_objects(static_obs)
                    scatter_plot.append([xpt,ypt])
        if clustering:
            thresh = (max_edge_ref/precision)*3
            clusters = hcluster.fclusterdata(scatter_plot, thresh, criterion="distance")
            list_of_clusters = list(set(clusters))
            for cluster_type in list_of_clusters:
                current_set = []
                for i in range(len(scatter_plot)):
                    if clusters[i] == cluster_type:
                        current_set.append(scatter_plot[i])
                    else:
                        pass
                if len(current_set) <=3:
                    for new_obs in current_set:
                        init_state_obs = State(time_step=0, orientation=0, position=np.array([0, 0]), velocity=0)
                        shape_obs = Circle(max_edge_ref/precision, center = np.array(new_obs))
                        static_obs = StaticObstacle(Scenario.generate_object_id(scenario), ObstacleType.LAND, obstacle_shape=shape_obs, initial_state=init_state_obs)
                        scenario.add_objects(static_obs)
                else:
                    hull = ConvexHull(current_set)
                    frontier = []
                    for index in hull.vertices:
                        frontier.append(current_set[index])
                    if frontier[0] != frontier[-1]:
                        frontier.append(frontier[0])

                    init_state_obs = State(time_step=0, orientation=0, position=np.array([0, 0]), velocity=0)
                    shape_obs = Polygon(np.array(frontier))
                    static_obs = StaticObstacle(Scenario.generate_object_id(scenario), ObstacleType.LAND, obstacle_shape=shape_obs, initial_state=init_state_obs)
                    scenario.add_objects(static_obs)
            
def addBuoys (buoys, scenario):
    """
    adds buoys to scenario
    """
    zone = []
    for buoy in buoys:
        lat = float(buoy.get('lat'))
        lon = float(buoy.get('lon'))
        coordinates = utm.from_latlon(lat,lon)
        UTMZone = [coordinates[2], coordinates[3]]
        if zone == []:
            zone = [coordinates[2], coordinates[3]]
        else:
            assert (zone[0] == UTMZone[0] and zone[1] == UTMZone[
                1]), 'Coordinates are in different UTM zones, {} != {}'.format(zone, UTMZone)

        type_buoy = TrafficSignElementID(getType(buoy))
        element = TrafficSignElement(type_buoy,[])
        sign = TrafficSign(Scenario.generate_object_id(scenario),[element],np.array([coordinates[0],coordinates[1]]))
        scenario.add_objects(sign, waters_ids=None)