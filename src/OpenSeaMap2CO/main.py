"""
This module can be executed to perform a conversion.
"""
import argparse
import os

import warnings
warnings.filterwarnings('ignore')

import matplotlib
from downloader import (
    download_around_map,
)

from ocean import convert_seamap

matplotlib.use("Qt5Agg")

def download_map(args):
    """
    downloads a map

    :return: None
    """
    x = args.latitude
    y = args.longitude
    if not os.path.exists(args.save_path):
        os.makedirs(args.save_path)
    download_around_map(
        args.save_path + args.benchmark_id + "_downloaded.osm",
        x,
        y,
        args.download_edge_length,
    )
    print(f"file saved as {args.save_path + args.benchmark_id}_downloaded.osm")

def main():
    parser = argparse.ArgumentParser(
        description="download or convert an OSM file to CommonOcean"
    )
    parser.add_argument("--download", "-d", help='download map', action='store_true')
    parser.add_argument("--file", nargs="?", default='files/ZAM_Test-1_1_T-1_downloaded.osm', help="file input for the converter")
    parser.add_argument("--seamap", "-s",help='convert Seamaps', action='store_true')
    parser.add_argument("--clustering", "-c",help='efficiently cluster adjustment points', action='store_true')
    parser.add_argument('--save_path', default='files/', type=str,
                        help='Path of the file to be saved')
    parser.add_argument("--benchmark_id", default="ZAM_Test-1_1_T-1", type=str,
                        help='Benchmark of the scenario')
    parser.add_argument("--download_edge_length", default=3000.0, type=float,
                        help='Length of the edge of the scenario')
    parser.add_argument("--latitude", default=8.80767, type=float,
                        help='Latitude of the center of the downloaded area')
    parser.add_argument("--longitude", default=-79.5168, type=float,
                        help='Longitude of the center of the downloaded area')
    parser.add_argument('--dt', default=1.0, type=float,
                        help='dt step of the new scenario')
    parser.add_argument('--precision', default=150, type=int,
                        help='precision of the granularity of the adjusments')

    args = parser.parse_args()
    if args.seamap and args.download:
        print("invalid operation")
    elif args.seamap:
        if not args.file:
            print("input without filename")
        else:
            convert_seamap(args.file, args.dt, args.save_path, args.precision, args.clustering)
            print(f"file saved as {args.save_path}OSM_Scenario.xml")
    elif args.download:
        download_map(args)
    else:
        print("invalid arguments")
        return


if __name__ == "__main__":
    main()
