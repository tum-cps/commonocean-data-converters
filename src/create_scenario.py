""" Creates a custom scenario from scratch """

__author__ = "Bruno Maione"
__copyright__ = "TUM Cyber-Physical Systems Group"
__credits__ = ["ConVeY"]
__version__ = "0.1"
__maintainer__ = "Hanna Krasowski"
__email__ = "hanna.krasowski@tum.de"
__status__ = "Release"

import subprocess
import argparse
from MarineCadastre2CO.MarineCadastre2AISscenario import LOCATION_DICT
import numpy as np

def get_args():

    parser = argparse.ArgumentParser(description="Script that generates a scenario from scratch using both the MarineCadastre dynamic data and OpenSeaMap static information")
    parser.add_argument('--location', default='', type=str,
                        help='Location of the scenario, given with x1,x2,y1,y2 or as predefined region: Florida, MiddleEastCoast, UpperWestCoast, NovaScotia')
    parser.add_argument('--marine_cadastre_csv_path', default='../../converter/MarineCadastre/AIS_2019_01_01.csv', type=str,
                        help='Path to the MarineCadastre csv raw scenario to be considered')
    parser.add_argument('--output_path', default='', type=str, help='Path to the folder of the output')
    parser.add_argument('--dt', default=1.0, type=float,
                        help='dt step of the new scenario')
    parser.add_argument('--reduceTimeframe', default=[], type=str, help='<Filtering step>: Reduce used timeframe, '
                                                                        'from given time + 1h, time given as '
                                                                        'Year-Month_DayTHour:Minute:Second')
    parser.add_argument('--scene', type=str,
                        help='<XML generation step>: scene identifier; format: XXX-0'
                             ' e.g. FLO-1 for open-sea area Florida')
    parser.add_argument('--author', type=str,
                        help='<XML generation step>: name of the author of the new scenario')
    parser.add_argument('--affiliation', type=str,
                        help='<XML generation step>: name of the affiliation of the new scenario')

    return parser.parse_args()


def main():
    args = get_args()
    print("0%")
    print("--------------------------------------------------------------------------------------")
    print("------------------------ Converting Marine Cadastre Data -----------------------------")
    print("--------------------------------------------------------------------------------------")
    if args.reduceTimeframe != []:
        subprocess.call(['python', 'src/MarineCadastre2CO/main.py', '--reduceLocation', args.location, '--generate_AIS_raw', 'True',
                        '--scenario_path', args.marine_cadastre_csv_path, '--dt', str(args.dt), '--reduceTimeframe', args.reduceTimeframe])
    else:
        subprocess.call(['python', 'src/MarineCadastre2CO/main.py', '--reduceLocation', args.location, '--generate_AIS_raw', 'True',
                        '--scenario_path', args.marine_cadastre_csv_path, '--dt', str(args.dt)])
    
    print("20%")
    print("--------------------------------------------------------------------------------------")
    print("------------------------- Creating XML Dynamic Scenario ------------------------------")
    print("--------------------------------------------------------------------------------------")
    print("40%")
    filename = args.marine_cadastre_csv_path
    if args.location != []:
        intermediate_path = filename.split('.')[0] + '_' + args.location + '_scenarios.csv'
    else:
        intermediate_path = filename.split('.')[0] + '_scenarios.csv'
    subprocess.call(['python', 'src/MarineCadastre2CO/main.py', '--scenario_path', intermediate_path, '--dt', str(args.dt),
                     '--author', args.author, '--scene', args.scene, '--affiliation', args.affiliation])
    print("--------------------------------------------------------------------------------------")
    print("-------------------------- Downloading OpenSeaMap Data -------------------------------")
    print("--------------------------------------------------------------------------------------")
    print("60%")

    if args.location in LOCATION_DICT.keys():
        center_lat = (LOCATION_DICT[args.location][0] + LOCATION_DICT[args.location][1])/2
        center_lon = (LOCATION_DICT[args.location][2] + LOCATION_DICT[args.location][3])/2
    else:
        red = np.fromstring(args.location, dtype= float, sep=',')
        center_lat = (red[0] + red[1])/2
        center_lon = (red[2] + red[3])/2
    subprocess.call(['python', 'src/OpenSeaMap2CO/main.py', '-d', '--latitude', str(center_lat), '--longitude', str(center_lon)])
    print("--------------------------------------------------------------------------------------")
    print("-------------------------- Converting OpenSeaMap to XML ------------------------------")
    print("--------------------------------------------------------------------------------------")
    subprocess.call(['python', 'src/OpenSeaMap2CO/main.py', '-s', '--dt', str(args.dt)])
    print("80%")
    print("--------------------------------------------------------------------------------------")
    print("-------------------------- Converting OpenSeaMap to XML ------------------------------")
    print("--------------------------------------------------------------------------------------")
    subprocess.call(['python', 'src/connector.py', '--open_sea_map_path', 'files/OSM_Scenario.xml' ,'--marine_cadastre_path', intermediate_path.replace('.csv', '.xml'), '--output_path', args.output_path, '--author', args.author, '--affiliation', args.affiliation])
    print("--------------------------------------------------------------------------------------")
    print(f"(100%) Done! Your scenario is in {args.output_path + '/Fusion_OpenSeaMap_MarineCadastre.xml'}")

    return

if __name__ == '__main__':
    main()

