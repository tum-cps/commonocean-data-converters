# CommonOcean Converters

This repository contains the CommonOcean data converters. The structure of the repository is:

```
.
├── src                                                # Converters for AIS data and sea maps
│   ├── MarineCadastre2CO                              # Marine Cadastre to CommonOcean converter
│   ├── OpenSeaMap2CO                                  # OpenSeaMap to CommonOcean converter
│   ├── connector.py                                   # Connector of outputs from Marine Cadastre and OpenSeaMap files into a single scenario
│   └── create_scenario.py                             # Creator of scenarios uniting the whole Marine Cadastre and OpenSeaMap processes
├── requirements.txt                                   # Required python packages
└── README                                             # Readme file             
```

## Preliminaries

To open and use the scripts in this repository, the commonocean\_io package has to be installed. Please follow the intructions [in the commonocean\_io repository](https://gitlab.lrz.de/tum-cps/commonocean-io).

In addition, the other required packages must be installed through requirements.txt and cartopy through conda
```
$ pip install -r requirements.txt
$ conda install -c conda-forge cartopy
```

## Converters

The convertes currently implement single purposes (i.e., get trajectories of ships from AIS data, get sea map in CommonOcean representation) but they can be combined if necessary. In the following, the functionalities of the two main converters MarineCadastre2CO and OpenSeaMap2CO are explained and how they can be combined is described.

> **_NOTE:_**  All the following codes are supposed to be used with the terminal in the basis folder of this repo. Running them in different paths may affect the outputs, especially considering the **create_scenario.py**.

### MarineCadastre2CO

This [file](./converter/MarineCadastre2CO/main.py) can be used to generate a CommonOcean scenario file (XML format) from AIS data of the [MarineCadastre dataset](https://marinecadastre.gov/ais/) for specific locations and times. The conversion is processed in two steps: filtering relevant AIS data and generating XML scenarios from the filtered data.

#### 1. Filtering relevant AIS data 

There are two ways to filter the AIS data. 

a) If you want to use all AIS data in a specific hour (here 9 am) and for a specific location. 

```bash
python src/MarineCadastre2CO/main.py --generate_AIS_raw True --reduceLocation '23.15,24.05,-96.67,-95.17'  --scenario_path '<path/to/your/AIS/file>' --reduceTimeframe '2022-02_20T09:00:00'
```

Be careful that you use the AIS data for the date you specified with the 'reduceTimeframe' argument. It is also possible to run the process for the entire day, without specifying the time.

b) If you want to search for close vessel encounters in a larger region you can use the following command to filter:

```bash
python src/MarineCadastre2CO/main.py --generate_AIS_raw True --reduceLocation '23.15,28.05,-96.67,-83.17'  --scenario_path '<path/to/your/AIS/file>' --vicinity_check
```

Next to individually specified regions we also support three predefined regions [min. latitude, max. latitude, min longitude, max. longitude] on the open sea:
* Florida: [27.51, 32.39, -80.18, -75.10] - scene: FLO-1
* Middle East Coast: [35.25, 38.89, -74.96, -73.92] - scene: MEC-1
* Upper West Coast: [37.32, 48.56, -126.91, -124.85] - scene: UWC-1

In case you want to search for close encounters for the region "Florida", you can run:

```bash
python src/MarineCadastre2CO/main.py --generate_AIS_raw True --reduceLocation Florida --scenario_path '<path/to/your/AIS/file>'
```

There are more parameters for the filtering than visible through the examples. Run the following to inspect their purpose:

```bash
python src/MarineCadastre2CO/main.py --help
```

#### 2. Generating an XML scenario

After getting this intermediate file, the last step is transform it to a XML scenario in CommonOcean format:

```bash
python src/MarineCadastre2CO/main.py --scenario_path '<path/to/your/intermediate/output/AIS/file>' --scene '<scene abbreviation>' --author '<your name>' --affiliation '<your affiliation>'
```

### OpenSeaMap2CO

This code is based on the [commonroad scenario designer](https://gitlab.lrz.de/tum-cps/commonroad-scenario-designer). To generate a CommonOcean map and store it in a scenario file, this [file](./src/OpenSeaMap2CO/main.py) can be used.

To generate the map for a location around latitude 40 and longitude -73, you need to specify
```
python src/OpenSeaMap2CO/main.py -d --download_edge_length 3000 --latitude 40.0 --longitude -73.0
python src/OpenSeaMap2CO/main.py -s
```

Considering that the API of OSM does not give outputs consistant with the restrictions of the size of our scenario, CommonOcean's converter has to apply some treatment over the data in order to obtain coastlines consitent with reality. To achieve this precise treatment, the **Basemap** module is used and every point of the scenario is checked to see if it is land or not, creating a circular static obstacle in each position with a proportional radius.

Even though this standard treatment gives good results in terms of precision, it is not the most efficient and there can be tiny blank spaces in the middle of land, depending on the precision used by the user. CommonOcean's converter presents, then, one alternative solution for those who want an efficient XML scenario at the end of the process by the cost of precision. The parameter **-c (clustering)** makes the script get all of these detected points, cluster them into groups (representing the parts of lands/islands) and get a convex frontier for all of them. To change the standard method for this last one, the input needs to be typed as

```
python src/OpenSeaMap2CO/main.py -d --download_edge_length 3000 --latitude 40.0 --longitude -73.0
python src/OpenSeaMap2CO/main.py -s -c
```

With this alternative approach, the XML file will be smaller (fewer obstacles than before), the tiny blank holes in the middle of lands will disappear but the coastlines will lose details and precision in general. It's up to the user to decide which is the best approach for the use taken.

### Combining AIS data and map data for CommonOcean scenarios

To assist in the process of creating and merging Marine Cadastre and OpenSeaMap data, two auxiliary scripts are available. They are connector.py (responsible for merging two Marine Cadastre and OpenSeaMap output XML files) and create_scenario.py (responsible for automating the entire process until the creation of a scenario, i.e., downloading and obtaining the two XMLs and then merging them).

An example of the full pipeline is
```
$ python src/create_scenario.py --location '27.51,32.39,-80.18,-75.10' --marine_cadastre_csv_path /path/to/the/AIS/file.csv --output_path /path/to/your/output/folder --scene '<scene abbreviation>' --author '<your name>' --affiliation '<your affiliation>'
```

# Contibutors and Reference

We thank all the contibutors for helping develop this project (see contibutors.txt).

**If you use our converter for research, please consider citing our paper:**
```
@inproceedings{Krasowski2022a,
	author = {Krasowski, Hanna and Althoff, Matthias},
	title = {CommonOcean: Composable Benchmarks for Motion Planning on Oceans},
	booktitle = {Proc. of the IEEE International Conference on Intelligent Transportation Systems},
	year = {2022},
}
```
